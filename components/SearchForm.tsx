'use client';
import Image from 'next/image';
import React, { useState } from 'react';
import { Input } from './ui/input';

const SearchForm = () => {
  const [Search, setSearch] = useState('');
  return (
    <form className="flex-center mx-auto mt-10 w-full sm:-mt-10 sm:px-5">
      <label className="flex-center relative w-full max-w-3xl">
        <Image src="magnifying-glass.svg" className="absolute left-8" width={32} height={32} alt="search"></Image>
        <Input
          type="text"
          placeholder="Search"
          className="base-regular py-6 text-white-800 pl-20 plaveholder:text-white-800 !ring-0 !ring-offset-0 pr-8 h-fit border-0 bg-black-400"
          value={Search}
          onChange={(e) => setSearch(e.target.value)}
        />
      </label>
    </form>
  );
};

export default SearchForm;

import React from 'react';

export const Footer = () => {
  return (
    <footer>
      <ul className="flex-between text-white w-full px-40 py-10 border-t bg-black-100 max-md:flex-col">
        <li>
          <p>Copyright &copy;</p>
        </li>
        <li>
          <p>Dominik Majcherczyk</p>
        </li>
      </ul>
    </footer>
  );
};

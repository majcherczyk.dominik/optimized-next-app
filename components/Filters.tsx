'use client';

import { cn } from '@/lib/utils';
import React from 'react';

const links = ['link1', 'link2', 'link3'];

const Filters = () => {
  const [active, setActive] = React.useState('');

  const handleClick = (link: string) => {
    setActive(link);
  };

  return (
    <ul className="text-white-800 body-text  flex w-full max-w-full gap-2 overflow-auto pt-12 pb-3 sm:max-w-2xl">
      {links.map((link) => (
        <button
          key={link}
          onClick={() => {
            handleClick(link);
          }}
          className={`${
            active === link ? 'gradient_blue-purple rounded-[25px]' : ''
          } whitespace-nowrap rouned-lg px-8 py-2.5 capitalize`}
        >
          {link}
        </button>
      ))}
    </ul>
  );
};

export default Filters;

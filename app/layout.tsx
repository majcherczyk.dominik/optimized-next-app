import type { Metadata } from 'next';
import './styles/globals.css';

export const metadata: Metadata = {
  title: 'optimized next app',
  description: 'optimized next app',
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className="bg-black-100 min-h-screen">{children}</body>
    </html>
  );
}
